  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Fixed Expenses</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="list_fixedexpenses">List Fixed Expenses</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
              <!-- /.card-header -->
              <!-- form start -->
             <!--  <form id="quickForm"> -->
              <form class="form form-horizontal form-bordered" action="<?php echo base_url();?>index.php/Fixedexpenses/save_fixedexpenses" id= "add_fixedexpenses"  method="post" enctype="multipart/form-data" autocomplete="off" novalidate>
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Store Name</label>
                    <input type="text" name="store_id" id="store_id" class="form-control" placeholder="Enter Store Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Store Rent</label>
                    <input type="number" name="store_rent" id="store_rent" class="form-control" placeholder="Enter Rent Amount">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Employee Name</label>
                    <input type="text" name="emp_id" id="emp_id" class="form-control"  placeholder="Enter Employee Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Electrical Bill Amount</label>
                   <textarea id="eb_amount" name="eb_amount" class="form-control"  placeholder="Enter Bill Amount"></textarea>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<script>
$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>
