

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ADD BANK DETAILS</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="list_bankdetails">List Bank Details</a></li>
               </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form form-horizontal form-bordered" action="<?php echo base_url();?>index.php/bankdetails/save_bankdetails" id= "add_bankdetails"  method="post" enctype="multipart/form-data" autocomplete="off" novalidate>
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Vendor Name</label>
                    <input type="text" name="vendor_id" class="form-control" id="vendor_id" placeholder="Vendor Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Bank Name</label>
                    <input type="text" name="bank_name" class="form-control" id="bank_name" placeholder="Bank Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Account Number</label>
                    <input type="number" name="account_no" class="form-control" id="account_no" placeholder="Account Number">
                  </div>
                  
                  <div class="form-group">
                    <label for="exampleInputPassword1">IFSC Code</label>
                    <input type="text" name="ifsc_code" class="form-control" id="ifsc_code" placeholder="IFSC Code">
                  </div>

                </div>

                <!-- general form elements disabled -->
            <div class="card card-warning">
              
              <!-- /.card-header -->
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      
                    </div>
                    
                  </div>
                  
                    
                  
                    
                  </div>

                  

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<script>
$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>
        
</body>
</html>
