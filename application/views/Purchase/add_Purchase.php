

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>NEW PURCHASE</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="list_purchase">List Purchase</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form id="quickForm">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Vendor Name</label>
                    <input type="text" name="vendor_id" class="form-control" id="vendor_id" placeholder="Vendor Name">
                  </div>

                  <h4>Product Category</h4>
                <div class="form-group">
                  
                  <select class="custom-select form-control-border" id="product_category">
                    <option>Chicken</option>
                    <option>Mutton</option>
                    <option>Seafood</option>
                    <option>Prawns&Crabs</option>
                    <option>Dry Fish</option>
                    <option>Pickels</option>
                    <option>Oil</option>
                    <option>Ready to fry</option>
                  </select>
                </div>
                 
                  <div class="form-group">
                    <label for="exampleInputPassword1">Product Qty</label>
                    <input type="number" name="pro_pruchase_qty" class="form-control" id="pro_pruchase_qty" placeholder="Product Qty">
                  </div>
                  
                  <div class="form-group">
                    <label for="exampleInputPassword1">Price</label>
                    <input type="number" name="each_price" class="form-control" id="each_price" placeholder="Price">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Price Total</label>
                    <input type="number" name="pro_total" class="form-control" id="pro_total" placeholder="Price Total">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Overall Total</label>
                    <input type="number" name="each_price" class="form-control" id="each_price" placeholder="Overall Total">
                  </div>




                </div>

                <!-- general form elements disabled -->
            <div class="card card-warning">
              
              <!-- /.card-header -->
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      
                    </div>
                    
                  </div>
                  
                    
                  
                    
                  </div>

                  

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<script>
$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>
        
</body>
</html>
