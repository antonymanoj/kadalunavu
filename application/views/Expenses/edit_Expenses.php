  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Expenses</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              
              <!-- /.card-header -->
              <!-- form start -->
             <!--  <form id="quickForm"> -->
              <form class="form form-horizontal form-bordered" action="<?php echo base_url();?>index.php/expenses/save_edit_expenses/<?php echo $editdata[0]->id;?>" id="edit_expenses"  method="post" enctype="multipart/form-data" autocomplete="off" novalidate>
                <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $editdata[0]->id;?>">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Store Name</label>
                    <input type="text" name="store_id" id="store_id" class="form-control" placeholder="Enter Vendor Name" value="<?php echo $editdata[0]->store_id ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Reason</label>
                    <input type="text" name="reason" id="reason" class="form-control" placeholder="Enter Company Name" value="<?php echo $editdata[0]->reason; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Amount Spent</label>
                    <input type="number" name="amount_spent" id="amount_spent" class="form-control"  placeholder="Enter Phone Number" value="<?php echo $editdata[0]->amount_spent; ?>">
                  </div>
                  <div class="form-group">
                  
                  <select class="custom-select form-control-border" name="payment_type"id="payment_type">
                    <option>Cash</option>
                    <option>Card</option>
                    <option>Online</option>
                  </select>
                </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
				  <button type="submit" class="btn btn-warning">Cancel</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<script>
$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>
