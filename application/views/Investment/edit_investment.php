  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Investment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
              <!-- /.card-header -->
              <!-- form start -->
             <!--  <form id="quickForm"> -->
              <form class="form form-horizontal form-bordered" action="<?php echo base_url();?>index.php/Investment/save_edit_investment/<?php echo $editdata[0]->id;?>" id="edit_investment"  method="post" enctype="multipart/form-data" autocomplete="off" novalidate>
                <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $editdata[0]->id;?>">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Partner Name</label>
                    <input type="text" name="partner_name" id="partner_name" class="form-control" placeholder="Enter Vendor Name" value="<?php echo $editdata[0]->partner_name ?>">
                  </div>
                  
                  <div class="form-group">
                    <label for="exampleInputPassword1">Phone Number</label>
                    <input type="number" name="partner_number" id="partner_number" min="10" max="10" class="form-control"  placeholder="Enter Phone Number" value="<?php echo $editdata[0]->partner_number; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Amount</label>
                   <textarea id="amount" name="amount" class="form-control"  placeholder="Enter Amount"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Total Amount</label>
                   <textarea id="total_amount_invested" name="total_amount_invested" class="form-control"  placeholder="Total Amount"></textarea>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
				  <button type="submit" class="btn btn-warning">Cancel</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<script>
$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>
