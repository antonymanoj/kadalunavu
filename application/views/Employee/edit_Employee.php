  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Employee</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
             
              <!-- /.card-header -->
              <!-- form start -->
             <!--  <form id="quickForm"> -->
              <form class="form form-horizontal form-bordered" action="<?php echo base_url();?>index.php/employee/save_edit_employee/<?php echo $editdata[0]->id;?>" id="edit_employee"  method="post" enctype="multipart/form-data" autocomplete="off" novalidate>
                <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $editdata[0]->id;?>">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Employee Name</label>
                    <input type="text" name="employee_name" id="employee_name" class="form-control" placeholder="Enter Employee Name" value="<?php echo $editdata[0]->employee_name ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Designation</label>
                    <input type="text" name="employee_designation" id="employee_designation" class="form-control" placeholder="Enter Designation" value="<?php echo $editdata[0]->employee_designation; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Employee Number</label>
                    <input type="number" name="employee_number" id="employee_number" class="form-control"  placeholder="Enter Emplyee Number" value="<?php echo $editdata[0]->employee_number; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1"> Address</label>
                   <textarea id="employee_address" name="employee_address" class="form-control"  placeholder="Enter Address"><?php echo $editdata[0]->employee_address;?></textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1"> Salary package</label>
                   <input type ="number" name="employee_salary" class="form-control" id="employee_salary"  placeholder="Salary Package" value="<?php echo $editdata[0]->employee_salary;?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1"> Date of joining</label>
                   <input type ="date" name="dob" class="form-control" id="dob"  placeholder="" value="<?php echo $editdata[0]->dob;?>">
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
				  <button type="submit" class="btn btn-warning">Cancel</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<script>
$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>
