

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>New Employee</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="list_employee">List Employee</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
	
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form form-horizontal form-bordered" action="<?php echo base_url();?>index.php/employee/save_employee" id= "add_employee"  method="post" enctype="multipart/form-data" autocomplete="off" novalidate>
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Enter Name</label>
                    <input type="text" name="employee_name" class="form-control" id="employee_name" placeholder="Enter Name">
                  </div>
                  
                  <div class="form-group">
                    <label for="exampleInputPassword1">Phone Number</label>
                    <input type="number" name="employee_number" class="form-control" id="employee_number" placeholder="Phone Number">
                  </div>
                  
                </div>

                <!-- general form elements disabled -->
            <div class="card card-warning">
              
              <!-- /.card-header -->
              <div class="card-body">
              
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      
                    </div>
                    
                  </div>
                  
                    <div class="form-group">
                      <!-- textarea -->
                      <div class="form-group">
                        <label>Address</label>
                       <textarea id="employee_address" name="employee_address" class="form-control"  placeholder="Address"></textarea>
                      </div>
                    </div>
                  
                    <div class="form-group">
                    <label for="exampleInputPassword1">Designation</label>
                    <input type="text" name="employee_designation" class="form-control" id="employee_designation" placeholder="Designation">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Salary package</label>
                    <input type="number" name="employee_salary" class="form-control" id="employee_salary" placeholder="Salary package">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Date Of Joining</label>
                    <input type="date" name="dob" class="form-control" id="dob" placeholder="">
                  </div>

                <!--   <div class="custom-file">
                    
                      <input type="file" class="custom-file-input" name="customFile "id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
 -->


                  </div>

                  

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<script>
$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>


 

</body>
</html>
