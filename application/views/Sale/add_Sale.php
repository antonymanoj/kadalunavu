

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Sale</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="list_Sale">List Sale</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

              <!-- /.card-header -->
              <!-- form start -->
              <form id="quickForm">
                <div class="card-body">

                  <h4>Sale Type</h4>
                <div class="form-group">
                  
                  <select class="custom-select form-control-border" id="product_cateogry">
                    <option>Online Sale</option>
                    <option>Whole Sale</option>
                    <option>Store Sale</option>
                    
                  </select>
                </div>

                <h4>Product Category</h4>
                <div class="form-group">
                  <select class="custom-select form-control-border" id="product_cateogry">
                    <option>Chicken</option>
                    <option>Mutton</option>
                    <option>Seafood</option>
                    <option>Prawns&Crabs</option>
                    <option>Dry Fish</option>
                    <option>Pickles</option>
                    <option>Oil</option>
                    <option>Ready to fry</option>
                  </select>
                </div>


                 
                  <div class="form-group">
                    <label for="exampleInputPassword1">Product Name</label>
                    <input type="text" name="product_name" class="form-control" id="product_name" placeholder="Product Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Qty</label>
                    <input type="number" name="qty" class="form-control" id="qty" placeholder="Qty">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Price</label>
                    <input type="number" name="each_price" class="form-control" id="each_price" placeholder="Price">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Total</label>
                    <input type="number" name="total" class="form-control" id="total" placeholder="Total">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Overall Total</label>
                    <input type="number" name="overall_total" class="form-control" id="overall_total" placeholder="Overall Total">
                  </div>
                  
                </div>
				
                 <script>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<script>
$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>
