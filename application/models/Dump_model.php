<?php
	class Dump_model extends CI_Model 
	{
		public function getAlldump(){
        $this->db->select("A.*");
        $this->db->from('dump_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_dump($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('dump_tbl');
			return true;
		}

		public function edit_sump($id){
        $this->db->select("A.*");
        $this->db->from('dump_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_dump($editdumpData,$edit_id){    
			$this->db->set($editdumpData);
			$this->db->where("id", $edit_id);     
			$this->db->update('dump_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}