<?php
	class Deliveryexpenses_model extends CI_Model 
	{
		public function getAlldeliveryexpenses(){
        $this->db->select("A.*");
        $this->db->from('delivery_expense_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_deliveryexpenses($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('delivery_expense_tbl');
			return true;
		}

		public function edit_deliveryexpenses($id){
        $this->db->select("A.*");
        $this->db->from('delivery_expense_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_deliveryexpenses($editdeliveryexpensesData,$edit_id){    
			$this->db->set($editdeliveryexpensesData);
			$this->db->where("id", $edit_id);     
			$this->db->update('delivery_expense_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}