<?php
	class Return_model extends CI_Model 
	{
		public function getAllreturnproducts(){
        $this->db->select("A.*");
        $this->db->from('return_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_returnproducts($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('return_tbl');
			return true;
		}

		public function edit_returnproducts($id){
        $this->db->select("A.*");
        $this->db->from('return_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_returnproducts($editreturnproductsData,$edit_id){    
			$this->db->set($editreturnproductsData);
			$this->db->where("id", $edit_id);     
			$this->db->update('return_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}