<?php
	class Pettycash_model extends CI_Model 
	{
		public function getAllpettycash(){
        $this->db->select("A.*");
        $this->db->from('petty_cash_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_pettycash($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('petty_cash_tbl');
			return true;
		}

		public function edit_pettycash($id){
        $this->db->select("A.*");
        $this->db->from('petty_cash_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_pettycash($editpettycashData,$edit_id){    
			$this->db->set($editpettycashData);
			$this->db->where("id", $edit_id);     
			$this->db->update('petty_cash_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}