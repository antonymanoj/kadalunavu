<?php
	class Expenses_model extends CI_Model 
	{
		public function getAllexpenses(){
        $this->db->select("A.*");
        $this->db->from('common_expenses_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_expenses($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('common_expenses_tbl');
			return true;
		}

		public function edit_expenses($id){
        $this->db->select("A.*");
        $this->db->from('common_expenses_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_expenses($editexpensesData,$edit_id){    
			$this->db->set($editexpensesData);
			$this->db->where("id", $edit_id);     
			$this->db->update('common_expenses_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}