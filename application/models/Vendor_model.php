<?php
	class Vendor_model extends CI_Model 
	{
		public function getAllVendors(){
        $this->db->select("A.*");
        $this->db->from('vendor_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_vendor($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('vendor_tbl');
			return true;
		}

		public function edit_vendor($id){
        $this->db->select("A.*");
        $this->db->from('vendor_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_vendor($editvendorData,$edit_id){    
			$this->db->set($editvendorData);
			$this->db->where("id", $edit_id);     
			$this->db->update('vendor_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}