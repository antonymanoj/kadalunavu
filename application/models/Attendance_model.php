<?php
	class Attendance_model extends CI_Model 
	{
		public function getAllattendance(){
        $this->db->select("A.*");
        $this->db->from('emp_attendance_tbl as A');
        $this->db->where('A.status',1); 
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_attendance($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('emp_attendance_tbl');
			return true;
		}

		public function edit_attendance($id){
        $this->db->select("A.*");
        $this->db->from('emp_attendance_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_attendance($editattendanceData,$edit_id){    
			$this->db->set($editattendanceData);
			$this->db->where("id", $edit_id);     
			$this->db->update('emp_attendance_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}