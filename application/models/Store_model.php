<?php
	class Store_model extends CI_Model 
	{
		public function getAllStore(){
        $this->db->select("A.*");
        $this->db->from('store_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_store($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('store_tbl');
			return true;
		}

		public function edit_store($id){
        $this->db->select("A.*");
        $this->db->from('store_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_store($editstoreData,$edit_id){    
			$this->db->set($editstoreData);
			$this->db->where("id", $edit_id);     
			$this->db->update('store_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}