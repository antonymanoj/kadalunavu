<?php
	class Investment_model extends CI_Model 
	{
		public function getAllInvestment(){
        $this->db->select("A.*");
        $this->db->from('investment_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_investment($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('investment_tbl');
			return true;
		}

		public function edit_investment($id){
        $this->db->select("A.*");
        $this->db->from('investment_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_investment($editinvestmentData,$edit_id){    
			$this->db->set($editinvestmentData);
			$this->db->where("id", $edit_id);     
			$this->db->update('investment_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}