<?php
	class Bankdetails_model extends CI_Model 
	{
		public function getAllbankdetails(){
        $this->db->select("A.*");
        $this->db->from('bank_details_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_bankdetails($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('bank_details_tbl');
			return true;
		}

		public function edit_bankdetails($id){
        $this->db->select("A.*");
        $this->db->from('bank_details_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_bankdetails($editbankdetailsData,$edit_id){    
			$this->db->set($editbankdetailsData);
			$this->db->where("id", $edit_id);     
			$this->db->update('bank_details_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}