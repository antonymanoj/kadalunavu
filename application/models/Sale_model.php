<?php
	class Sale_model extends CI_Model 
	{
		public function getAllsale(){
        $this->db->select("A.*");
        $this->db->from('sales_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_sale($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('sales_tbl');
			return true;
		}

		public function edit_sale($id){
        $this->db->select("A.*");
        $this->db->from('sales_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_sale($editsaleData,$edit_id){    
			$this->db->set($editsaleData);
			$this->db->where("id", $edit_id);     
			$this->db->update('sales_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}