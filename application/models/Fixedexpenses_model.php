<?php
	class Fixedexpenses_model extends CI_Model 
	{
		public function getAllfixedexpenses(){
        $this->db->select("A.*");
        $this->db->from('fixed_expense_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_fixedexpenses($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('fixed_expense_tbl');
			return true;
		}

		public function edit_fixedexpenses($id){
        $this->db->select("A.*");
        $this->db->from('fixed_expense_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_fixedexpenses($editfixedexpensesData,$edit_id){    
			$this->db->set($editfixedexpensesData);
			$this->db->where("id", $edit_id);     
			$this->db->update('fixed_expense_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}