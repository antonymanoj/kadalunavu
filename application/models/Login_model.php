<?php
	class Login_model extends CI_Model 
	{
		var $execute_query    = array();
		var $um_return_value  = array();
		var $count_result     = NULL;
		// public function __construct()
		// {
		// 	parent::__construct();
		// }
		public function updateUserPassword($newPassword)  
		{  
			$user_id=$this->session->userdata('user_id'); 
			$modifiedDate = date("Y-m-d H:i:s");
			$data = array(  
			'UD.modified_by'         =>      $user_id,
			'UD.modified_date'        =>      $modifiedDate,
			'UD.user_password'        =>      encrypt(trim($newPassword))); 
			$this->db->set($data); 
			$this->db->where("UD.user_id", $user_id);  
			$res=$this->db->update("user_details as UD"); 
			return $res; 
		}
		public function getUserPassword(){
			$user_id=$this->session->userdata('user_id'); 
			$this->db->select('user_password');
			$this->db->from('user_details');
			$this->db->where("user_id", $user_id);
			$query = $this->db->get();
			return $query->row_array();
		}
		public  function changeUserPassword($user_name,$new_password)  
		{  
			$modifiedDate = date("Y-m-d H:i:s");
			$data = array(  
			'UD.modified_by' => $this->session->userdata('user_id'),
			'UD.modified_date' => $modifiedDate,
			'UD.user_password' => encrypt(trim($new_password))
			);
			$this->db->set($data); 
			$this->db->where("UD.user_code", $user_name); 
			$res=$this->db->update("user_details as UD"); 
			return $res; 
		}
		public function checkUsername($user_name)
		{
			$this->db->select('user_code');
			$this->db->from('user_details');
			$this->db->where('user_code', $user_name);
			$query = $this->db->get();
			if($query->num_rows()==1) {
				return true;
			}
			else { 
				return false;
			}
		}
		public function getDeletedUsers(){
			$this->db->select('*');
			$this->db->from('user_details as U');
			$this->db->where(array('U.is_delete'=> 1, 'U.is_active'=> 0));
			$query = $this->db->get();
			return $query->num_rows();
		}
		public function getTotalInActiveUsers(){
			$this->db->select('*');
			$this->db->from('user_details as U');
			$this->db->where(array('U.is_active'=> 0));
			$query = $this->db->get();
			return $query->num_rows();
		}
		public function getTotalUsers(){
			$this->db->select('*');
			$this->db->from('user_details as U');
			$query = $this->db->get();
			return $query->num_rows();
		}
		public function getTotalActiveUsers(){
			$this->db->select('*');
			$this->db->from('user_details as U');
			$this->db->where(array('U.is_active'=> 1));
			$query = $this->db->get();
			return $query->num_rows();
		}
		public function getAdminLogin($post)
		{
			// print(base64_encode($post["password"]));die;
			if(!empty($post) && is_array($post))
			{
				// print_r($post);die;
				$this->db->select("A.*");
				$this->db->from('register_tbl as A');
				$this->db->where('A.reg_email',    trim($post["reg_email"])); 
				$this->db->where('A.password',  base64_encode($post["password"])); 
				$this->db->where('A.status', "1"); 
				$query = $this->db->get();
				 //echo $this->db->last_query();die;
				if($query->num_rows() == 1) {
					foreach ($query->result() as $row){
						$usersession = array(
						'reg_email'          =>     $row->reg_email,	
						'first_name'         =>     $row->first_name,
						'id'                 =>     $row->id,
						'created_at'         =>     $row->created_at,
						'status'             =>     $row->status
						);
						$this->session->set_userdata($usersession);
					}
					return "valid";
				} 
				else{
					return "invalid";
				}
			}
			else{
				return "invalid";
			}
		}
		public function getSettings(){
			$this->db->select('*');
			$this->db->from('settings as S');
			$this->db->where(array('S.is_active' => 1));
			$query = $this->db->get();
			return $query->row();
		}
		public function logout()
		{
			$user_id =$this->session->userdata('user_id');
			$last = $this->db->where('login_id',$user_id)->order_by('login_id',"desc")->limit(1)->get('tm_user_logs')->row();
			$date=date("Y-m-d H:i:s");
			$logoutDate = array('logout_date' => $date, 'status'=>'offline');
			$this->db->set($logoutDate);
			$this->db->where('login_id',$last->login_id);
			$this->db->where('login_history_id',$last->login_history_id);
			$this->db->update('tm_user_logs');
			return true;
		}
	}