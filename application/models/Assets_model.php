<?php
	class Assets_model extends CI_Model 
	{
		public function getAllAssets(){
        $this->db->select("A.*");
        $this->db->from('assets_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_assets($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('assets_tbl');
			return true;
		}

		public function edit_assets($id){
        $this->db->select("A.*");
        $this->db->from('assets_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_assets($editassetsData,$edit_id){    
			$this->db->set($editassetsData);
			$this->db->where("id", $edit_id);     
			$this->db->update('assets_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}

		public function get_all_active_store_names()
		{
			$this->db->select('S.store_name,S.id');
			$this->db->from('store_tbl  S');
			$this->db->where('S.status', '1');
			// $this->db->limit(1); 
			$query = $this->db->get();
			 // echo $this->db->last_query();die;
			return $query->result();
		}
    	
	}