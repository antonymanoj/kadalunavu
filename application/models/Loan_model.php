<?php
	class Loan_model extends CI_Model 
	{
		public function getAllloan(){
        $this->db->select("A.*");
        $this->db->from('loans_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_loan($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('loans_tbl');
			return true;
		}

		public function edit_loan($id){
        $this->db->select("A.*");
        $this->db->from('loans_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_loan($editloanData,$edit_id){    
			$this->db->set($editloanData);
			$this->db->where("id", $edit_id);     
			$this->db->update('loans_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}