<?php
	class Employee_model extends CI_Model 
	{
		public function getAllemployee(){
        $this->db->select("A.*");
        $this->db->from('emp_detail_tbl as A');
        $this->db->where('A.status',1);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

    	public function delete_employee($id){
			$this->db->set('status',0);
			$this->db->where('id',$id);
			$this->db->update('emp_detail_tbl');
			return true;
		}

		public function edit_employee($id){
        $this->db->select("A.*");
        $this->db->from('emp_detail_tbl as A');
        $this->db->where('A.status',1);  
        $this->db->where('A.id',$id);  
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    	}

		public function save_edit_employee($editemployeeData,$edit_id){    
			$this->db->set($editemployeeData);
			$this->db->where("id", $edit_id);     
			$this->db->update('emp_detail_tbl');
			 //echo $this->db->last_query();die;
			//return true;         
		}
    	
	}