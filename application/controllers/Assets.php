<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assets extends CI_Controller {

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_Assets()
	{
		$data['store_name']  =   $this->Assets_model->get_all_active_store_names();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Assets/add_Assets',$data);
		$this->load->view('layout/footer');
	}

public function save_assets()
	{
		$assetsdata       = array(
		'product_name'     => $this->input->post('product_name'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'product_qty'    => $this->input->post('product_qty'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		$assets = $this->db->insert('assets_tbl',$assetsdata);
		if(empty($assets))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('assets/add_assets');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Assets is created successfully');
                $data['assetslist']  =  $this->Assets_model->getAllassets();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('assets/list_assets',$data);
				$this->load->view('layout/footer');
			}
	}
	public function list_assets()
	{
		$data['assetslist']  =  $this->Assets_model->getAllAssets();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('assets/list_assets',$data);
		$this->load->view('layout/footer');
	}
public function delete_assets($id){
			$delete=$this->Assets_model->delete_assets($id);
			$data['assetslist']  =  $this->Assets_model->getAllassets();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('assets/list_assets',$data);
			$this->load->view('layout/footer');
	}

	public function edit_assets($id){
		    $data['store_name']  =   $this->Assets_model->get_all_active_store_names();
			$data['editdata'] = $this->Assets_model->edit_assets($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('assets/edit_assets',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_assets()
	{
		$edit_id = $this->input->post('edit_id');
		$editassetsData = array(
			'product_name'     => $this->input->post('product_name'),
			'product_qty'    => $this->input->post('product_qty'),
	
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['assetslist']  =  $this->Assets_model->save_edit_assets($editassetsData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['assetslist']  =  $this->Assets_model->getAllassets();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('assets/list_assets',$data);
			$this->load->view('layout/footer');
		}


}

