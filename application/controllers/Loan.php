<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan extends CI_Controller {

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_loan()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Loan/add_loan');	
		$this->load->view('layout/footer');
	}

	public function save_loan()
	{
		$loandata       = array(
		'creditordebit'     => $this->input->post('creditordebit'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'loan_name'    => $this->input->post('loan_name'),
		'loan_amount'    => $this->input->post('loan_amount'),
		'contact_details'         => $this->input->post('contact_details'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		
		$loan = $this->db->insert('loans_tbl',$loandata);
		if(empty($loan))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('Loan/add_loan');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, loan is created successfully');
                $data['loanlist']  =  $this->Loan_model->getAllloan();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('Loan/list_loan',$data);
				$this->load->view('layout/footer');
			}
	}

	public function list_loan()
	{
		$data['loanlist']  =  $this->Loan_model->getAllloan();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('loan/list_loan',$data);
		$this->load->view('layout/footer');
	}

	public function delete_loan($id){
			$delete=$this->Loan_model->delete_loan($id);
			$data['loanlist']  =  $this->Loan_model->getAllloan();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('loan/list_loan',$data);
			$this->load->view('layout/footer');
	}

	public function edit_loan($id){
			$data['editdata'] = $this->Loan_model->edit_loan($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('Loan/edit_loan',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_loan()
	{
		$edit_id = $this->input->post('edit_id');
		$editloanData = array(
			'creditordebit'     => $this->input->post('creditordebit'),
			'loan_name'    => $this->input->post('loan_name'),
			'loan_amount'         => $this->input->post('loan_amount'),
			'contact_details'    => $this->input->post('contact_details'),
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['loanlist']  =  $this->Loan_model->save_edit_loan($editloanData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['loanlist']  =  $this->Loan_model->getAllloan();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('Loan/list_loan',$data);
			$this->load->view('layout/footer');
		}


}