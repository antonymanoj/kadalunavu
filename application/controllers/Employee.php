<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_Employee()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('employee/add_employee');
		$this->load->view('layout/footer');
	}

	public function save_employee()
	{
		$employeedata       = array(

		'employee_name'     => $this->input->post('employee_name'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'employee_address'    => $this->input->post('employee_address'),
		'employee_number'    => $this->input->post('employee_number'),
		'employee_designation'       => $this->input->post('employee_designation'),
		'employee_salary'       => $this->input->post('employee_salary'),
		'dob'       => $this->input->post('dob'),
		//'employee_document'       => $this->input->post('choose_file'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		//print_r($employeedata);die;
		$employee = $this->db->insert('emp_detail_tbl',$employeedata);
		if(empty($employee))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('employee/add_employee');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Employee is created successfully');
                $data['employeelist']  =  $this->Employee_model->getAllemployee();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('employee/list_employee',$data);
				$this->load->view('layout/footer');
			}
	}


	public function list_Employee()
	{
		$data['employeelist']  =  $this->Employee_model->getAllEmployee();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('employee/list_employee',$data);
		$this->load->view('layout/footer');
	}


public function delete_employee($id){
			$delete=$this->Employee_model->delete_employee($id);
			$data['employeelist']  =  $this->Employee_model->getAllemployee();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('employee/list_employee',$data);
			$this->load->view('layout/footer');
	}

	public function edit_employee($id){
			$data['editdata'] = $this->Employee_model->edit_employee($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('employee/edit_employee',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_employee()
	{
		$edit_id = $this->input->post('edit_id');
		$editemployeeData = array(
			'employee_name'     => $this->input->post('employee_name'),
		'employee_address'    => $this->input->post('address'),
		'employee_number'    => $this->input->post('employee_number'),
		'employee_designation'       => $this->input->post('employee_designation'),
		'employee_salary'       => $this->input->post('employee_salary'),
		'dob'       => $this->input->post('dob'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['employeelist']  =  $this->Employee_model->save_edit_employee($editemployeeData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['employeelist']  =  $this->Employee_model->getAllemployee();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('employee/list_employee',$data);
			$this->load->view('layout/footer');
		}


}