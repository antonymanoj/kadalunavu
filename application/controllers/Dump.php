<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dump extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_Dump()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Dump/add_Dump');
		$this->load->view('layout/footer');
	}
public function save_dump()
	{
		$dumpdata       = array(
		'store_id'     => $this->input->post('store_id'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'dump_tbl_product_name'    => $this->input->post('dump_tbl_product_name'),
		'dump_tbl_qty'    => $this->input->post('dump_tbl_qty'),
		'dump_tbl_reason'         => $this->input->post('dump_tbl_reason'),
		//'dump_tbl_image'         => $this->input->post('dump_tbl_image'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		$dump = $this->db->insert('dump_tbl',$dumpdata);
		if(empty($dump))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('dump/add_dump');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Dump is created successfully');
                $data['dumplist']  =  $this->Dump_model->getAlldump();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('dump/list_dump',$data);
				$this->load->view('layout/footer');
			}
	}
	public function delete_dump($id){
			$delete=$this->Dump_model->delete_dump($id);
			$data['dumplist']  =  $this->Dump_model->getAlldump();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('dump/list_dump',$data);
			$this->load->view('layout/footer');
	}

	public function edit_dump($id){
			$data['editdata'] = $this->Dump_model->edit_dump($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('dump/edit_dump',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_dump()
	{
		$edit_id = $this->input->post('edit_id');
		$editdumpData = array(
			'store_id'     => $this->input->post('store_id'),
			'dump_tbl_product_name'    => $this->input->post('dump_tbl_product_name'),
			'dump_tbl_qty'         => $this->input->post('dump_tbl_qty'),
			'dump_tbl_reason'    => $this->input->post('dump_tbl_reason'),
			//'dump_tbl_image'    => $this->input->post('dump_tbl_image'),
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['vendorlist']  =  $this->Dump_model->save_edit_dump($editdumpData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['dumplist']  =  $this->Dump_model->getAlldump();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('dump/list_dump',$data);
			$this->load->view('layout/footer');
		}





	public function list_Dump()
	{
		$data['dumplist']  =  $this->Dump_model->getAllDump();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Dump/list_Dump',$data);
		$this->load->view('layout/footer');
	}
}
