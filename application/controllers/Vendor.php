<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_vendor()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('vendor/add_vendor');	
		$this->load->view('layout/footer');
	}

	public function save_vendor()
	{
		$vendordata       = array(
		'vendor_name'     => $this->input->post('vendor_name'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'company_name'    => $this->input->post('company_name'),
		'phone_number'    => $this->input->post('phone_number'),
		'address'         => $this->input->post('address'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		
		$vendor = $this->db->insert('vendor_tbl',$vendordata);
		if(empty($vendor))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('vendor/add_vendor');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Vendor is created successfully');
                $data['vendorlist']  =  $this->Vendor_model->getAllVendors();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('vendor/list_vendor',$data);
				$this->load->view('layout/footer');
			}
	}

	public function list_vendor()
	{
		$data['vendorlist']  =  $this->Vendor_model->getAllVendors();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('vendor/list_vendor',$data);
		$this->load->view('layout/footer');
	}

	public function delete_vendor($id){
			$delete=$this->Vendor_model->delete_vendor($id);
			$data['vendorlist']  =  $this->Vendor_model->getAllVendors();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('vendor/list_vendor',$data);
			$this->load->view('layout/footer');
	}

	public function edit_vendor($id){
			$data['editdata'] = $this->Vendor_model->edit_vendor($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('vendor/edit_vendor',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_vendor()
	{
		$edit_id = $this->input->post('edit_id');
		$editvendorData = array(
			'vendor_name'     => $this->input->post('vendor_name'),
			'company_name'    => $this->input->post('company_name'),
			'address'         => $this->input->post('address'),
			'phone_number'    => $this->input->post('phone_number'),
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['vendorlist']  =  $this->Vendor_model->save_edit_vendor($editvendorData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['vendorlist']  =  $this->Vendor_model->getAllVendors();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('vendor/list_vendor',$data);
			$this->load->view('layout/footer');
		}


}