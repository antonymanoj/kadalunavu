<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}


	public function add_store()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('store/add_store');
		$this->load->view('layout/footer');
	}


public function save_store()
	{
		$storedata       = array(
		'store_name'     => $this->input->post('store_name'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'incharge_name'    => $this->input->post('incharge_name'),
		'incharge_number'    => $this->input->post('incharge_number'),
		'store_address'         => $this->input->post('store_address'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		// print_r($storedata);die;
		$store = $this->db->insert('store_tbl',$storedata);
		if(empty($store))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('store/add_store');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Store is created successfully');
                $data['storelist']  =  $this->Store_model->getAllstore();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('store/list_store',$data);
				$this->load->view('layout/footer');
			}
	}

	public function list_store()
	{
		$data['storelist']  =  $this->Store_model->getAllStore();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('store/list_store',$data);
		$this->load->view('layout/footer');
	}
public function delete_store($id){
			$delete=$this->Store_model->delete_store($id);
			$data['storelist']  =  $this->Store_model->getAllStore();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('store/list_store',$data);
			$this->load->view('layout/footer');
	}

	public function edit_store($id){
			$data['editdata'] = $this->Store_model->edit_store($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('store/edit_store',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_store()
	{
		$edit_id = $this->input->post('edit_id');
		$editstoreData = array(
			'store_name'     => $this->input->post('store_name'),
			'incharge_name'    => $this->input->post('incharge_name'),
			'store_address'         => $this->input->post('store_address'),
			'incharge_number'    => $this->input->post('incharge_number'),
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($storeData);die();
			$data['storelist']  =  $this->Store_model->save_edit_store($editstoreData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['storelist']  =  $this->Store_model->getAllStore();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('store/list_store',$data);
			$this->load->view('layout/footer');
		}


}


