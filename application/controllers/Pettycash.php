<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pettycash extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_Pettycash()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Pettycash/add_Pettycash');
		$this->load->view('layout/footer');
	}

	public function save_pettycash()
	{
		$pettycashdata       = array(
		'store_id'     => $this->input->post('store_id'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'weekly_amount'    => $this->input->post('weekly_amount'),
		//'payment_type'    => $this->input->post('payment_type'),
		
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		$pettycash = $this->db->insert('petty_cash_tbl',$pettycashdata);
		if(empty($pettycash))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('pettycash/add_pettycash');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Pettycash is created successfully');
                $data['pettycashlist']  =  $this->Pettycash_model->getAllpettycash();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('pettycash/list_pettycash',$data);
				$this->load->view('layout/footer');
			}

}
	public function list_Pettycash()
	{
		$data['pettycashlist']  =  $this->Pettycash_model->getAllPettycash();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Pettycash/list_Pettycash',$data);
		$this->load->view('layout/footer');
	}
	public function delete_pettycash($id){
			$delete=$this->Pettycash_model->delete_pettycash($id);
			$data['pettycashlist']  =  $this->Pettycash_model->getAllpettycash();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('pettycash/list_pettycash',$data);
			$this->load->view('layout/footer');
	}

	public function edit_pettycash($id){
			$data['editdata'] = $this->Pettycash_model->edit_pettycash($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('pettycash/edit_pettycash',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_pettycash()
	{
		$edit_id = $this->input->post('edit_id');
		$editpettycashData = array(
			'store_id'     => $this->input->post('store_id'),
			'weekly_amount'    => $this->input->post('weekly_amount'),
			//'payment_type'         => $this->input->post('payment_type'),
			
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['pettycashlist']  =  $this->Pettycash_model->save_edit_pettycash($editpettycashData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['pettycashlist']  =  $this->Pettycash_model->getAllpettycash();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('pettycash/list_pettycash',$data);
			$this->load->view('layout/footer');
		}
}
