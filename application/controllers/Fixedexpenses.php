<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fixedexpenses extends CI_Controller {

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_fixedexpenses()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Fixedexpenses/add_fixedexpenses');	
		$this->load->view('layout/footer');
	}

	public function save_fixedexpenses()
	{
		$fixedexpensesdata       = array(
		'store_id'     => $this->input->post('store_id'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'store_rent'    => $this->input->post('store_rent'),
		'emp_id'    => $this->input->post('emp_id'),
		'eb_amount'         => $this->input->post('eb_amount'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		
		$fixedexpenses = $this->db->insert('fixed_expense_tbl',$fixedexpensesdata);
		if(empty($fixedexpenses))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('Fixedexpenses/add_fixedexpenses');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Vendor is created successfully');
                $data['fixedexpenseslist']  =  $this->Fixedexpenses_model->getAllfixedexpenses();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('Fixedexpenses/list_fixedexpenses',$data);
				$this->load->view('layout/footer');
			}
	}

	public function list_fixedexpenses()
	{
		$data['fixedexpenseslist']  =  $this->Fixedexpenses_model->getAllfixedexpenses();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Fixedexpenses/list_fixedexpenses',$data);
		$this->load->view('layout/footer');
	}

	public function delete_fixedexpenses($id){
			$delete=$this->Fixedexpenses_model->delete_fixedexpenses($id);
			$data['fixedexpenseslist']  =  $this->Fixedexpenses_model->getAllfixedexpenses();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('fixedexpenses/list_fixedexpenses',$data);
			$this->load->view('layout/footer');
	}

	public function edit_fixedexpenses($id){
			$data['editdata'] = $this->Fixedexpenses_model->edit_fixedexpenses($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('fixedexpenses/edit_fixedexpenses',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_fixedexpenses()
	{
		$edit_id = $this->input->post('edit_id');
		$editfixedexpensesData = array(
			'store_id'     => $this->input->post('store_id'),
			'store_rent'    => $this->input->post('store_rent'),
			'emp_id'         => $this->input->post('emp_id'),
			'eb_amount'    => $this->input->post('eb_amount'),
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['fixedexpenseslist']  =  $this->Fixedexpenses_model->save_edit_fixedexpenses($editfixedexpensesData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['fixedexpenseslist']  =  $this->Fixedexpenses_model->getAllfixedexpenses();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('Fixedexpenses/list_fixedexpenses',$data);
			$this->load->view('layout/footer');
		}


}