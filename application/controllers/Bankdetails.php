<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bankdetails extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_Bankdetails()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Bankdetails/add_Bankdetails');
		$this->load->view('layout/footer');
	}
	public function save_bankdetails()
	{
		$bankdetailsdata       = array(
		'vendor_id'     => $this->input->post('vendor_id'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'bank_name'    => $this->input->post('bank_name'),
		'account_no'    => $this->input->post('account_no'),
		'ifsc_code'         => $this->input->post('ifsc_code'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		$bankdetails = $this->db->insert('bank_details_tbl',$bankdetailsdata);
		if(empty($bankdetails))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('bankdetails/add_bankdetails');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Bank Details is created successfully');
                $data['bankdetailslist']  =  $this->Bankdetails_model->getAllbankdetails();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('bankdetails/list_bankdetails',$data);
				$this->load->view('layout/footer');
			}
		}

	public function list_bankdetails()
	{
		$data['bankdetailslist']  =  $this->Bankdetails_model->getAllbankdetails();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('bankdetails/list_bankdetails',$data);
		$this->load->view('layout/footer');
	}

	public function delete_bankdetails($id){
			$delete=$this->Bankdetails_model->delete_bankdetails($id);
			$data['bankdetailslist']  =  $this->Bankdetails_model->getAllbankdetails();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('bankdetails/list_bankdetails',$data);
			$this->load->view('layout/footer');
	}

	public function edit_bankdetails($id){
			$data['editdata'] = $this->Bankdetails_model->edit_bankdetails($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('bankdetails/edit_bankdetails',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_bankdetails()
	{
		$edit_id = $this->input->post('edit_id');
		$editbankdetailsData = array(
			'vendor_id'     => $this->input->post('vendor_id'),
			'bank_name'    => $this->input->post('bank_name'),
			'account_no'         => $this->input->post('account_no'),
			'ifsc_code'    => $this->input->post('ifsc_code'),
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['bankdetailslist']  =  $this->Bankdetails_model->save_edit_bankdetails($editbankdetailsData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['bankdetailslist']  =  $this->Bankdetails_model->getAllbankdetails();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('bankdetails/list_bankdetails',$data);
			$this->load->view('layout/footer');
		}

}
