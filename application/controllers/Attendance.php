<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_Attendance()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Attendance/add_Attendance');
		$this->load->view('layout/footer');
	}
public function save_attendance()
	{
		$attendancedata       = array(
		'store_id'     => $this->input->post('store_id'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'emp_id'    => $this->input->post('emp_id'),
		//'emp_in_out'    => $this->input->post('emp_in_out'),
		//'emp_in_out'         => $this->input->post('emp_in_out'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		$attendance = $this->db->insert('emp_attendance_tbl',$attendancedata);
		if(empty($attendance))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('attendance/add_attendance');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Attendance is created successfully');
                $data['attendancelist']  =  $this->Attendance_model->getAllattendance();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('attendance/list_attendance',$data);
				$this->load->view('layout/footer');
			}
	}


	public function list_Attendance()
	{
		$data['attendancelist']  =  $this->Attendance_model->getAllAttendance();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Attendance/list_Attendance',$data);
		$this->load->view('layout/footer');
	}
	public function delete_attendance($id){
			$delete=$this->Attendance_model->delete_attendance($id);
			$data['attendancelist']  =  $this->Attendance_model->getAllattendance();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('attendance/list_attendance',$data);
			$this->load->view('layout/footer');
	}

	public function edit_attendance($id){
			$data['editdata'] = $this->Attendance_model->edit_attendance($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('attendance/edit_attendance',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_attendance()
	{
		$edit_id = $this->input->post('edit_id');
		$editattendanceData = array(
			'store_id'     => $this->input->post('venstore_iddor_name'),
			'emp_id'    => $this->input->post('emp_id'),
			//'emp_in_out'         => $this->input->post('emp_in_out'),
			//'emp_in_out'    => $this->input->post('emp_in_out'),
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['attendancelist']  =  $this->Attendance_model->save_edit_attendance($editattendanceData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['attendancelist']  =  $this->Attendance_model->getAllattendance();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('attendance/list_attendance',$data);
			$this->load->view('layout/footer');
		}


}

