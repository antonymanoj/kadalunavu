<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	// public function __construct(){
	// 		parent::__construct();
	// 	}
	public function index()
	{
		$this->load->view('accounts/login');
	}

	public function logincheck(){ 
		 // print_r($this->input->post());die;
			if($this->input->post()){
				$response = $this->Login_model->getAdminLogin($this->input->post());
				if($response=="valid"){
			 		$this->session->set_flashdata('greenmsg', 'Login success');
					$this->load->view('layout/menu');
					$this->load->view('layout/header');
					$this->load->view('accounts/dashboard');
					$this->load->view('layout/footer');
				}
				else{
					$this->session->set_flashdata('redmsg', 'Invalid username or password, Please try again');
					$this->load->view('accounts/login');
				}
			}
			
		}

		public function dashboard(){
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('accounts/dashboard');
			$this->load->view('layout/footer');
		}

		public function register()
		{
			$this->load->view('accounts/register');	
		}

		public function saveregister()
		{
			$regdata      = array(
			'first_name'       => $this->input->post('first_name'),
			'reg_email'        => $this->input->post('reg_email'),
			'password'         => base64_encode($this->input->post('password')),
			'created_at'       => date("Y-m-d H:i:s"),
			'status'           => 1, 
			'is_deleted'       => 'no'
			);
			$register = $this->db->insert('register_tbl',$regdata);
			if(empty($register))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('accounts/register');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Employee is created successfully');
			    $this->load->view('accounts/login');	
			}
		}
}
