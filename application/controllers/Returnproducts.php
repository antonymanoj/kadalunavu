<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Returnproducts extends CI_Controller {

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_returnproducts()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Returnproducts/add_returnproducts');	
		$this->load->view('layout/footer');
	}

	public function save_returnproducts()
	{
		$returnproductsdata       = array(
		'store_id'     => $this->input->post('store_id'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'product_name'    => $this->input->post('product_name'),
		'product_qty'    => $this->input->post('product_qty'),
		
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		
		$returnproducts = $this->db->insert('return_tbl',$returnproductsdata);
		if(empty($returnproducts))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('Returnproducts/add_returnproducts');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Return is created successfully');
                $data['returnproductslist']  =  $this->Return_model->getAllreturnproducts();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('Returnproducts/list_returnproducts',$data);
				$this->load->view('layout/footer');
			}
	}

	public function list_returnproducts()
	{
		$data['returnproductslist']  =  $this->Return_model->getAllreturnproducts();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Returnproducts/list_returnproducts',$data);
		$this->load->view('layout/footer');
	}

	public function delete_returnproducts($id){
			$delete=$this->Return_model->delete_returnproducts($id);
			$data['returnproductslist']  =  $this->Return_model->getAllreturnproducts();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('Returnproducts/list_returnproducts',$data);
			$this->load->view('layout/footer');
	}

	public function edit_returnproducts($id){
			$data['editdata'] = $this->Return_model->edit_returnproducts($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('Returnproducts/edit_returnproducts',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_returnproducts()
	{
		$edit_id = $this->input->post('edit_id');
		$editreturnData = array(
			'store_id'     => $this->input->post('store_id'),
			'product_name'    => $this->input->post('product_name'),
			'product_qty'         => $this->input->post('product_qty'),
			
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['returnproductslist']  =  $this->Return_model->save_edit_returnproducts($editreturnproductsData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['returnproductslist']  =  $this->Return_model->getAllreturnproducts();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('Returnproducts/list_returnproducts',$data);
			$this->load->view('layout/footer');
		}


}