<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Investment extends CI_Controller {

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_investment()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Investment/add_investment');	
		$this->load->view('layout/footer');
	}

	public function save_investment()
	{
		$investmentdata       = array(
		'partner_name'     => $this->input->post('partner_name'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		
		'partner_number'    => $this->input->post('partner_number'),
		'amount'         => $this->input->post('amount'),
		'total_amount_invested'         => $this->input->post('total_amount_invested'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		
		$investment = $this->db->insert('investment_tbl',$investmentdata);
		if(empty($investment))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('Investment/add_investment');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, investment is Added successfully');
                $data['investmentlist']  =  $this->Investment_model->getAllinvestment();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('Investment/list_investment',$data);
				$this->load->view('layout/footer');
			}
	}

	public function list_investment()
	{
		$data['investmentlist']  =  $this->Investment_model->getAllinvestment();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Investment/list_investment',$data);
		$this->load->view('layout/footer');
	}

	public function delete_investment($id){
			$delete=$this->Investment_model->delete_investment($id);
			$data['investmentlist']  =  $this->Investment_model->getAllinvestment();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('Investment/list_investment',$data);
			$this->load->view('layout/footer');
	}

	public function edit_investment($id){
			$data['editdata'] = $this->Investment_model->edit_investment($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('Investment/edit_investment',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_investment()
	{
		$edit_id = $this->input->post('edit_id');
		$editinvestmentData = array(
			'partner_name'     => $this->input->post('partner_name'),
		
			'amount'         => $this->input->post('amount'),
		'total_amount_invested'         => $this->input->post('total_amount_invested'),
			'partner_number'    => $this->input->post('partner_number'),
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['investmentlist']  =  $this->Investment_model->save_edit_investment($editinvestmentData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['investmentlist']  =  $this->Investment_model->getAllinvestment();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('Investment/list_investment',$data);
			$this->load->view('layout/footer');
		}


}