<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deliveryexpenses extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_Deliveryexpenses()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Deliveryexpenses/add_Deliveryexpenses');
		$this->load->view('layout/footer');
	}
public function save_Deliveryexpenses()
	{
		$deliveryexpensesdata       = array(
		'store_id'     => $this->input->post('store_id'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'order_number'    => $this->input->post('order_number'),
		'delivery_amount'    => $this->input->post('delivery_amount'),
		
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		$deliveryexpenses = $this->db->insert('delivery_expense_tbl',$deliveryexpensesdata);
		if(empty($deliveryexpenses))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('deliveryexpenses/add_deliveryexpenses');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Delivery Expenses is created successfully');
                $data['deliveryexpenseslist']  =  $this->Deliveryexpenses_model->getAlldeliveryexpenses();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('deliveryexpenses/list_deliveryexpenses',$data);
				$this->load->view('layout/footer');
			}
	}

	public function list_Deliveryexpenses()
	{
		$data['deliveryexpenseslist']  =  $this->Deliveryexpenses_model->getAllDeliveryexpenses();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Deliveryexpenses/list_Deliveryexpenses',$data);
		$this->load->view('layout/footer');
	}
}
