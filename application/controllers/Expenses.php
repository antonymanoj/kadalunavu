<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expenses extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}

	public function add_Expenses()
	{
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Expenses/add_Expenses');
		$this->load->view('layout/footer');
	}
public function save_expenses()
	{
		$expensesdata       = array(
		'store_id'     => $this->input->post('store_id'),//right side la irukuthu  form la iruka name.
		//left side la irukurathu db la iruka clm name.
		'reason'    => $this->input->post('reason'),
		'amount_spent'    => $this->input->post('amount_spent'),
		//'payment_type'         => $this->input->post('payment_type'),
	    'created_at'      => date("Y-m-d H:i:s"),
	    'created_by'      => $this->session->userdata('id'),
	    'status'          => 1,//Active
	    'is_deleted'      => 'no'
		);
		$expenses = $this->db->insert('common_expenses_tbl',$expensesdata);
		if(empty($expenses))
            {
				$this->session->set_flashdata('redmsg', 'Warning, Something went wrong');
				$this->load->view('expenses/add_expenses');	
			}
            else
            {
                $this->session->set_flashdata('greenmsg', 'Success, Expenses is created successfully');
                $data['expenseslist']  =  $this->Expenses_model->getAllexpenses();
				$this->load->view('layout/menu');
				$this->load->view('layout/header');
				$this->load->view('expenses/list_expenses',$data);
				$this->load->view('layout/footer');
			}
	}

	public function list_Expenses()
	{
		$data['expenseslist']  =  $this->Expenses_model->getAllExpenses();
		$this->load->view('layout/menu');
		$this->load->view('layout/header');
		$this->load->view('Expenses/list_Expenses',$data);
		$this->load->view('layout/footer');
	}
	public function delete_expenses($id){
			$delete=$this->Expenses_model->delete_expenses($id);
			$data['expenseslist']  =  $this->Expenses_model->getAllexpenses();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('expenses/list_expenses',$data);
			$this->load->view('layout/footer');
	}

	public function edit_expenses($id){
			$data['editdata'] = $this->Expenses_model->edit_expenses($id);
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('expenses/edit_expenses',$data);
			$this->load->view('layout/footer');
	}

	public function save_edit_expenses()
	{
		$edit_id = $this->input->post('edit_id');
		$editexpensesData = array(
			'store_id'     => $this->input->post('store_id'),
			'reason'    => $this->input->post('reason'),
			'amount_spent'         => $this->input->post('amount_spent'),
			//'payment_type'    => $this->input->post('payment_type'),
			'updated_at'      => date("Y-m-d H:i:s"),
			'updated_by'      => $this->session->userdata('id')
			);
		// echo "<pre>";
		// 	print_r($vendorData);die();
			$data['expenseslist']  =  $this->Expenses_model->save_edit_expenses($editexpensesData,$edit_id);
			$this->session->set_flashdata('msg', 'Category added');
			$data['expenseslist']  =  $this->Expenses_model->getAllexpenses();
			$this->load->view('layout/menu');
			$this->load->view('layout/header');
			$this->load->view('expenses/list_expenses',$data);
			$this->load->view('layout/footer');
		}


}

